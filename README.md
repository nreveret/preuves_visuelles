# Preuves visuelles

Différentes illustrations de résultats mathématiques ou de concepts informatiques illustrées avec [Manim](https://www.manim.community/).

Le site est accessible sur [nreveret.forge.apps.education.fr/preuves_visuelles/](https://nreveret.forge.apps.education.fr/preuves_visuelles/).