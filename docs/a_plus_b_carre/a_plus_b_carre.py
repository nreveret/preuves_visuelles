from manim import *


class Identite(Scene):
    def construct(self):
        titre = MathTex(r"(a+b)^2=\,?").move_to(3 * UP).scale(1.5)
        self.play(Write(titre), run_time=2)

        a = 3
        b = 1

        # Carré a+b
        carre_a_b = Square(side_length=a + b, color=PURPLE, fill_opacity=0.75).move_to(3 * LEFT + 1 * DOWN)
        texte_a_b = MathTex(r"(a+b)^2").scale(1.25).move_to(carre_a_b)
        accolade_a_b_gauche = BraceLabel(carre_a_b, "a+b", LEFT)
        accolade_a_b_haut = BraceLabel(carre_a_b, "a+b", UP)
        self.play(
            GrowFromPoint(carre_a_b, titre.get_center()),
            run_time=2,
        )
        self.wait(1)
        self.play(
            GrowFromEdge(accolade_a_b_gauche, UP),
            GrowFromEdge(accolade_a_b_haut, LEFT),
            run_time=2,
        )
        self.wait(1)
        self.play(
            GrowFromCenter(texte_a_b),
            run_time=2,
        )
        self.wait(1)

        # Carré a
        carre_a = Square(side_length=a, color=GREEN, fill_opacity=0.75).move_to(3 * RIGHT).align_to(carre_a_b, UP)
        carre_a.add(MathTex("a^2").scale(1.25).move_to(carre_a))
        accolade_a_gauche = BraceLabel(carre_a, "a", LEFT)
        accolade_a_haut = BraceLabel(carre_a, "a", UP)
        self.play(
            GrowFromPoint(carre_a, LEFT),
            GrowFromEdge(accolade_a_gauche, UP),
            GrowFromEdge(accolade_a_haut, LEFT),
            run_time=2,
        )
        self.wait(1)
        # Rectangles ab

        ## bas
        traqueur = ValueTracker(0.01)
        rectangle_ab_bas = (
            Rectangle(height=b, color=ORANGE, fill_opacity=0.75).next_to(carre_a, DOWN, buff=0).align_to(carre_a, LEFT)
        )
        rectangle_ab_bas.add_updater(lambda m: m.stretch_to_fit_width(traqueur.get_value()))
        rectangle_ab_bas.add_updater(lambda m: m.align_to(carre_a, LEFT))
        texte_rectangle_ab_bas = MathTex("ab").scale(1.25).move_to(rectangle_ab_bas)
        texte_rectangle_ab_bas.add_updater(lambda t: t.move_to(rectangle_ab_bas))
        accolade_b_gauche = BraceLabel(rectangle_ab_bas, "b", LEFT)
        self.add(rectangle_ab_bas)
        self.play(
            traqueur.animate.set_value(a),
            FadeIn(texte_rectangle_ab_bas),
            GrowFromEdge(accolade_b_gauche, LEFT),
            run_time=2,
        )
        rectangle_ab_bas.clear_updaters()
        texte_rectangle_ab_bas.clear_updaters()
        rectangle_ab_bas.add(texte_rectangle_ab_bas)
        self.wait(1)

        ## droite
        traqueur.set_value(0.01)
        rectangle_ab_droite = (
            Rectangle(width=b, color=ORANGE, fill_opacity=0.75).next_to(carre_a, RIGHT, buff=0).align_to(carre_a, UP)
        )
        rectangle_ab_droite.add_updater(lambda m: m.stretch_to_fit_height(traqueur.get_value()))
        rectangle_ab_droite.add_updater(lambda m: m.align_to(carre_a, UP))
        texte_rectangle_ab_droite = MathTex("ab").scale(1.25).move_to(rectangle_ab_droite)
        texte_rectangle_ab_droite.add_updater(lambda t: t.move_to(rectangle_ab_droite))
        accolade_b_haut = BraceLabel(rectangle_ab_droite, "b", UP)
        self.add(rectangle_ab_droite)
        self.play(
            traqueur.animate.set_value(a),
            FadeIn(texte_rectangle_ab_droite),
            GrowFromEdge(accolade_b_haut, UP),
            run_time=2,
        )
        rectangle_ab_droite.clear_updaters()
        texte_rectangle_ab_droite.clear_updaters()
        rectangle_ab_droite.add(texte_rectangle_ab_droite)
        self.wait(1)

        # Carré b^2
        carre_b = Square(side_length=b, color=BLUE, fill_opacity=0.75).next_to(
            rectangle_ab_bas, RIGHT, buff=0
        )
        carre_b.add(MathTex("b^2").scale(1.25).move_to(carre_b))
        self.play(GrowFromCenter(carre_b), run_time=2)
        self.wait(1)

        groupe = VGroup(carre_a, rectangle_ab_bas, rectangle_ab_droite, carre_b)

        self.play(
            FadeOut(texte_a_b),
            FadeOut(accolade_a_b_gauche),
            FadeOut(accolade_a_b_haut),
            FadeOut(accolade_a_gauche),
            FadeOut(accolade_a_haut),
            FadeOut(accolade_b_gauche),
            FadeOut(accolade_b_haut),
            run_time=1,
        )

        self.play(
            groupe.animate.move_to(DOWN),
            FadeOut(carre_a_b, target_position=DOWN),
            run_time=2,
        )

        titre_2 = MathTex(r"(a+b)^2=").move_to(3 * UP + 2 * LEFT).scale(1.5)
        self.play(ReplacementTransform(titre, titre_2))

        a_carre = (
            Square(side_length=a, color=GREEN, fill_opacity=0.75)
            .scale(0.25)
            .next_to(titre_2, RIGHT)
            .align_to(titre, DOWN)
        )
        self.play(GrowFromPoint(a_carre, carre_a.get_center()), run_time=2)

        plus_1 = MathTex("+").next_to(a_carre, RIGHT).scale(1.5)
        ab_1 = Rectangle(width=b, height=a, color=ORANGE, fill_opacity=0.75).scale(0.5).next_to(plus_1, RIGHT)
        self.play(Write(plus_1), GrowFromPoint(ab_1, rectangle_ab_droite.get_center()), run_time=2)

        plus_2 = MathTex("+").next_to(ab_1, RIGHT).scale(1.5)
        ab_2 = Rectangle(width=a, height=b, color=ORANGE, fill_opacity=0.75).scale(0.5).next_to(plus_2, RIGHT)
        self.play(Write(plus_2), GrowFromPoint(ab_2, rectangle_ab_bas.get_center()), run_time=2)

        plus_3 = MathTex("+").next_to(ab_2, RIGHT).scale(1.5)
        b_carre = Square(side_length=b, color=BLUE, fill_opacity=0.75).scale(0.5).next_to(plus_3, RIGHT)
        self.play(Write(plus_3), GrowFromPoint(b_carre, carre_b.get_center()), run_time=2)
        self.wait(1)

        self.play(Rotate(ab_2, PI / 2), run_time=1)
        dx = plus_2.get_right() + 0.5 * RIGHT - ab_2.get_left()
        self.play(
            ab_2.animate.shift(2 * dx),
            plus_3.animate.shift(4 * dx),
            b_carre.animate.shift(4 * dx),
            run_time=1,
        )
        self.wait(1)

        texte_aa = MathTex("a^2").move_to(a_carre).shift(0.1 * UP).scale(1.5)
        texte_ab_1 = MathTex("ab").move_to(ab_1).scale(1.5)
        texte_ab_2 = MathTex("ab").move_to(ab_2).scale(1.5)
        texte_bb = MathTex("b^2").move_to(b_carre).shift(0.05 * UP).scale(1.5)
        self.play(
            Transform(a_carre, texte_aa, replace_mobject_with_target_in_scene=True),
            Transform(ab_1, texte_ab_1, replace_mobject_with_target_in_scene=True),
            Transform(ab_2, texte_ab_2, replace_mobject_with_target_in_scene=True),
            Transform(b_carre, texte_bb, replace_mobject_with_target_in_scene=True),
            run_time=2,
        )
        self.wait(1)

        groupe_2 = VGroup(titre_2, texte_aa, plus_1, texte_ab_1, plus_2, texte_ab_2, plus_3, texte_bb)
        conclusion = MathTex(r"(a+b)^2=a^2+2ab+b^2").scale(1.5)
        self.play(
            FadeOut(groupe),
            # FadeOut(groupe_2),
            TransformMatchingTex(groupe_2, conclusion),
            run_time=2,
        )
        self.wait(2)
        self.play(ApplyWave(conclusion))
        self.wait(1)
