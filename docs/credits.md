---
author: Nicolas Revéret
title: Crédits
---

# 👍🏽 Crédits

Ce site a été réalisé par N. Revéret. Les fichiers sont disponibles sur [ce dépôt](https://forge.apps.education.fr/nreveret/preuves_visuelles).

L'ensemble des documents sont sous licence [CC-BY-NC-SA 4.0 (Attribution, Utilisation Non Commerciale, ShareAlike)](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

Les logos ont été créés par [Freepik - Flaticon](https://www.flaticon.com/free-icons/end-to-end)
