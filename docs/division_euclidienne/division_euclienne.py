from manim import *


class Division(Scene):
    def construct(self):
        self.wait(0.5)

        # Présentation
        titre = Text("Division euclidienne").scale(1.5)
        self.play(Write(titre, run_time=3))
        self.wait(0.5)
        self.play(FadeOut(titre, run_time=1, shift=3 * UP))

        # Réglages
        echelle_rectangles = 0.35
        dividende = 23  # dvd dans la suite
        diviseur = 5  # dvs dans la suite
        hauteur_rectangles = 2.5
        opacite_rectangles = 0.8
        couleur_dvd = BLUE_D
        couleur_dvs = ORANGE

        # Exemple
        division_exemple = MathTex(rf"{dividende} : {diviseur} =\,?").move_to(3 * UP).scale(2)
        self.play(Write(division_exemple, run_time=3))

        # Rectangle 29
        rectangle_dvd = (
            Rectangle(couleur_dvd, height=hauteur_rectangles, width=dividende, fill_opacity=opacite_rectangles)
            .scale(echelle_rectangles)
            .move_to(0.5 * UP)
        )
        texte_dvd = Text(str(dividende)).move_to(rectangle_dvd)
        groupe_dvd = VGroup(rectangle_dvd, texte_dvd)
        self.play(GrowFromPoint(groupe_dvd, division_exemple.get_left()))

        rectangle_dvs = Rectangle(
            couleur_dvs, height=hauteur_rectangles, width=diviseur, fill_opacity=opacite_rectangles
        ).scale(echelle_rectangles)
        texte_dvs = Text(str(diviseur)).move_to(rectangle_dvs)
        rectangle_dvs = VGroup(rectangle_dvs, texte_dvs).next_to(groupe_dvd, 6 * DOWN).align_to(groupe_dvd, LEFT)
        ligne_depart = DashedLine(groupe_dvd.get_corner(DL), rectangle_dvs.get_corner(UL))
        c, d, _ = rectangle_dvd.get_corner(DR)
        a, b, _ = rectangle_dvs.get_corner(DR)
        ligne_fin = DashedLine([c, d, 0], [c, b, 0])

        self.play(
            GrowFromPoint(rectangle_dvs, division_exemple.get_center()),
            Create(ligne_depart),
            run_time=2
        )
        self.wait(1)

        # Animation division 1
        x, y, z = rectangle_dvs.get_corner(UR)
        _, y_dvd, _ = groupe_dvd.get_bottom()
        ligne_quotient = DashedLine([x, y_dvd, z], [x, y, z])
        accolade_qotient = BraceLabel(rectangle_dvs, r"1 \times 5=5", DOWN)
        rectangle_reste = Rectangle(
            GRAY, height=hauteur_rectangles, width=dividende - diviseur, fill_opacity=opacite_rectangles
        ).scale(echelle_rectangles)
        rectangle_reste.next_to(rectangle_dvs, RIGHT, buff=0)
        texte_reste = Text(str(dividende-diviseur)).move_to(rectangle_reste)
        accolade_reste = BraceLabel(rectangle_reste, rf"{dividende-diviseur}", DOWN)
        self.play(
            Create(ligne_quotient),
            Create(ligne_fin),
            GrowFromEdge(accolade_qotient, LEFT),
            GrowFromEdge(rectangle_reste, LEFT),
            GrowFromEdge(texte_reste, LEFT),
            GrowFromEdge(accolade_reste, LEFT),
            run_time=2
        )
        self.wait(0.5)

        # Animation division 2
        rectangle_dvs_2 = rectangle_dvs.copy().next_to(rectangle_dvs, RIGHT, buff=0)
        x, y, z = rectangle_dvs_2.get_corner(UR)
        ligne_quotient_2 = DashedLine([x, y_dvd, z], [x, y, z])
        accolade_qotient_2 = BraceLabel(VGroup(rectangle_dvs, rectangle_dvs_2), r"2 \times 5=10", DOWN)
        rectangle_reste_2 = Rectangle(
            GRAY, height=hauteur_rectangles, width=dividende - 2 * diviseur, fill_opacity=opacite_rectangles
        ).scale(echelle_rectangles)
        rectangle_reste_2.next_to(rectangle_dvs_2, RIGHT, buff=0)
        texte_reste_2 = Text(str(dividende-2*diviseur)).move_to(rectangle_reste_2)
        accolade_reste_2 = BraceLabel(rectangle_reste_2, rf"{dividende-2*diviseur}", DOWN)
        self.play(
            GrowFromEdge(rectangle_dvs_2, LEFT),
            ReplacementTransform(ligne_quotient, ligne_quotient_2),
            ReplacementTransform(accolade_qotient, accolade_qotient_2),
            ReplacementTransform(rectangle_reste, rectangle_reste_2),
            ReplacementTransform(texte_reste, texte_reste_2),
            ReplacementTransform(accolade_reste, accolade_reste_2),
            run_time=2
        )
        self.wait(0.5)

        # Animation division 3
        rectangle_dvs_3 = rectangle_dvs.copy().next_to(rectangle_dvs_2, RIGHT, buff=0)
        x, y, z = rectangle_dvs_3.get_corner(UR)
        ligne_quotient_3 = DashedLine([x, y_dvd, z], [x, y, z])
        accolade_qotient_3 = BraceLabel(VGroup(rectangle_dvs, rectangle_dvs_3), r"3 \times 5=15", DOWN)
        rectangle_reste_3 = Rectangle(
            GRAY, height=hauteur_rectangles, width=dividende - 3 * diviseur, fill_opacity=opacite_rectangles
        ).scale(echelle_rectangles)
        rectangle_reste_3.next_to(rectangle_dvs_3, RIGHT, buff=0)
        texte_reste_3 = Text(str(dividende-3*diviseur)).move_to(rectangle_reste_3)
        accolade_reste_3 = BraceLabel(rectangle_reste_3, rf"{dividende-3*diviseur}", DOWN)
        self.play(
            GrowFromEdge(rectangle_dvs_3, LEFT),
            ReplacementTransform(ligne_quotient_2, ligne_quotient_3),
            ReplacementTransform(accolade_qotient_2, accolade_qotient_3),
            ReplacementTransform(rectangle_reste_2, rectangle_reste_3),
            ReplacementTransform(texte_reste_2, texte_reste_3),
            ReplacementTransform(accolade_reste_2, accolade_reste_3),
            run_time=2
        )
        self.wait(0.5)

        # Animation division 4
        rectangle_dvs_4 = rectangle_dvs.copy().next_to(rectangle_dvs_3, RIGHT, buff=0)
        x, y, z = rectangle_dvs_4.get_corner(UR)
        ligne_quotient_4 = DashedLine([x, y_dvd, z], [x, y, z])
        accolade_qotient_4 = BraceLabel(VGroup(rectangle_dvs, rectangle_dvs_4), r"4 \times 5=20", DOWN)
        rectangle_reste_4 = Rectangle(
            GRAY, height=hauteur_rectangles, width=dividende - 4 * diviseur, fill_opacity=opacite_rectangles
        ).scale(echelle_rectangles)
        rectangle_reste_4.next_to(rectangle_dvs_4, RIGHT, buff=0)
        texte_reste_4 = Text(str(dividende-4*diviseur)).move_to(rectangle_reste_4)
        accolade_reste_4 = BraceLabel(rectangle_reste_4, rf"{dividende-4*diviseur}", DOWN)
        self.play(
            GrowFromEdge(rectangle_dvs_4, LEFT),
            ReplacementTransform(ligne_quotient_3, ligne_quotient_4),
            ReplacementTransform(accolade_qotient_3, accolade_qotient_4),
            ReplacementTransform(rectangle_reste_3, rectangle_reste_4),
            ReplacementTransform(texte_reste_3, texte_reste_4),
            ReplacementTransform(accolade_reste_3, accolade_reste_4),
            run_time=2
        )
        self.wait(0.5)

        # Conclusion 1
        division_conclusion = MathTex(rf"{dividende} : {diviseur} =4 \times 5 + 3").move_to(3 * UP).scale(2)
        self.play(
            TransformMatchingTex(division_exemple, division_conclusion),
            TransformMatchingShapes(accolade_reste_4, division_conclusion),
            TransformMatchingShapes(accolade_qotient_4, division_conclusion),
            run_time=2
        )

        self.wait(1)

        formes = [
            rectangle_dvd,
            texte_dvd,
            rectangle_dvs,
            rectangle_dvs_2,
            rectangle_dvs_3,
            rectangle_dvs_4,
            ligne_depart,
            ligne_quotient_4,
            rectangle_reste_4,
            texte_reste_4,
            ligne_fin
        ]

        self.play(*[FadeOut(mob) for mob in formes])

        # Conclusion 2
        texte_quotient = Tex("Le quotient est 4").scale(1.75)
        texte_reste = Tex("Le reste est 3").move_to(2 * DOWN).scale(1.75)
        self.play(
            GrowFromPoint(texte_quotient, division_conclusion.get_center()),
            GrowFromPoint(texte_reste, division_conclusion.get_center()),
            run_time=2
        )
        self.wait(2)
        texte_quotient_P = Tex(r"Avec Python : \texttt{23 // 5} vaut \texttt{4}").move_to(texte_quotient).scale(1.75)
        texte_reste_P = Tex(r"Avec Python : \texttt{23 \% 5} vaut \texttt{3}").move_to(texte_reste).scale(1.75)
        self.play(
            TransformMatchingTex(texte_quotient, texte_quotient_P),
            TransformMatchingTex(texte_reste, texte_reste_P),
            run_time=2
        )

        self.wait(3)
