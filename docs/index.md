---
author: Nicolas Revéret
title: Preuves visuelles
---

# Preuves visuelles

Vous trouverez ci-dessous différentes vidéos illustrant des résultats mathématiques ou notions informatiques.

Ces vidéos ont été réalisées avec [Manim](https://www.manim.community/).

------------------------------------------------

## Identités remarquables

![type:video](./a_plus_b_carre/media/videos/a_plus_b_carre/480p15/Identite.mp4)

------------------------------------------------

## Division euclidienne

![type:video](./division_euclidienne/media/videos/division_euclienne/480p15/Division.mp4)

------------------------------------------------

## Somme des premiers entiers

![type:video](./somme_1_N/media/videos/somme_1_N/480p15/Somme.mp4)

------------------------------------------------

## Recherche dichotomique

![type:video](./recherche_dichot/media/videos/dicho/480p15/DichotomieSucces.mp4)
