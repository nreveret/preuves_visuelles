from manim import *

PAUSE = 0.5
DUREE_WRITE = 3
A = 2
C = 5
B = np.sqrt(C * C - A * A)


def pause(delai=PAUSE):
    self.wait(delai)


class Pythagore(Scene):
    def pause(self, delai=PAUSE):
        self.wait(delai)

    def construct(self):
        # Le titre
        titre = Text("Théorème de Pythagore")
        self.play(Write(titre), run_time=DUREE_WRITE)
        self.pause()
        self.play(FadeOut(titre, run_time=1, shift=3 * UP))
        self.next_section()
        # Aire d'un carré
        titre = Text("Aire d'un carré").move_to(3 * UP).scale(1.25)
        self.play(Write(titre), run_time=DUREE_WRITE)
        self.pause()
        segment = Line(start=[-C / 2, -3, 0], end=[C / 2, -3, 0], color=BLUE_A)
        points = [Dot(point=[-C / 2 + k, -3, 0], color=BLUE_A, z_index=1) for k in range(0, 6)]
        commentaire_segment = MathTex("a = 5").next_to(segment, DOWN).scale(1.5)
        self.play(Create(segment), Write(commentaire_segment), *[Create(p) for p in points])
        self.pause()

        carre = Square(side_length=5, color=BLUE_A, fill_color=BLUE_D, fill_opacity=0.8).rotate(PI)
        carre.move_to(segment.get_center() + (C / 2) * UP)
        self.play(DrawBorderThenFill(carre), run_time=DUREE_WRITE)
        self.pause()

        segments = [Line(start=[-C / 2 + k, -3, 0], end=[-C / 2 + k, -3 + C, 0], color=BLUE_C) for k in range(1, C)]

        segments.extend([Line(start=[-C / 2, k, 0], end=[C / 2, k, 0], color=BLUE_C) for k in range(-2, -3 + C)])
        self.play(*[Create(ligne) for ligne in segments], run_time=2)
        self.pause()

        texte_aire = MathTex(r"5 \times 5").move_to(carre).scale(2)
        self.play(*[FadeOut(ligne) for ligne in segments], *[FadeOut(p) for p in points], Write(texte_aire), run_time=2)
        self.pause(2 * PAUSE)

        a_fois_a = MathTex(r"a \times a").move_to(carre).scale(2)
        self.play(ReplacementTransform(texte_aire, a_fois_a))
        self.pause()
        a_carre = MathTex(r"a^2").move_to(carre).scale(2)
        self.play(ReplacementTransform(a_fois_a, a_carre))
        self.pause()

        self.play(FadeOut(*self.mobjects))
        self.pause()
        self.next_section()

        # Le moulin à vent
        coeff_reduction = 0.5
        triangle = Polygon(*[[0, 0, 0], [B, 0, 0], [B, A, 0]], color=BLUE_A, fill_color=BLUE_D, fill_opacity=0.8)
        angle_droit = Square(side_length=0.5)
        angle_droit.move_to([B - 0.25, + 0.25, 0])
        triangle_rectangle = VGroup(triangle, angle_droit)
        triangle_rectangle.rotate(-np.arctan(A / B) - PI)
        triangle_rectangle.scale(coeff_reduction).move_to(ORIGIN)
        triangle_rectangle.z_index = 1
        s_A, s_B, s_C = list(triangle.get_vertices())
        a = MathTex("a", z_index=1).scale(1.25)
        a.move_to((s_B + s_C) / 2 + 0.2 * LEFT)
        b = MathTex("b", z_index=1).scale(1.25)
        b.move_to((s_A + s_B) / 2 + 0.4 * UP)
        c = MathTex("c", z_index=1).scale(1.25)
        c.move_to((s_A + s_C) / 2 + 0.4 * DOWN)


        self.play(DrawBorderThenFill(triangle_rectangle), run_time=3)
        self.play(
            LaggedStart(
                Write(a),
                Write(b),
                Write(c),
                lag_ratio=1,
            )
        )
        self.pause()

        # carre_a = Square(side_length=A, color=ORANGE, fill_color=ORANGE, fill_opacity=0.8).scale(0.75)
        # carre_a.rotate(np.arctan(B/A))
        # carre_a.move_to((s_B + s_C) / 2 + ((s_B - s_A) / C) * (A / 2))
        # carre_a = Polygon(s_B, s_C, s_C + A * (s_B-s_A) / C, s_B + A * (s_B-s_A) / C,color=ORANGE, fill_color=ORANGE, fill_opacity=0.8)
        carre_a = Square(side_length=A).scale(coeff_reduction)
        carre_a.rotate(np.arctan(B/A))
        carre_a.next_to(triangle.get_vertices()[2], direction=RIGHT, buff=0)
        carre_b = Polygon(s_B, s_A, s_A + B * (s_B-s_C) / A, s_B + B * (s_B-s_C) / A,color=GREEN, fill_color=GREEN, fill_opacity=0.8)
        carre_c = Polygon(s_C, s_A, s_A + C * coeff_reduction * DOWN, s_C + C * coeff_reduction * DOWN,color=RED, fill_color=RED, fill_opacity=0.8)
        self.add(carre_a, carre_b, carre_c)