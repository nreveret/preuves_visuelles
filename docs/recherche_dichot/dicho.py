from manim import *
from random import randrange, sample


class DichotomieSucces(Scene):
    def creation_tableau(self, tableau):
        couleurs = color_gradient([BLUE, ORANGE], len(tableau))
        cases = []
        for i, x in enumerate(tableau):
            s = Square(color=couleurs[i], fill_opacity=0.75).scale(0.6).add(Text(str(x)))
            if i > 0:
                s.next_to(cases[-1], RIGHT, buff=0).align_to(cases[-1], UP)
            t = Text(str(i), font="Consolas").scale(0.75).next_to(s, DOWN)
            cases.append(VGroup(s, t))
        return cases

    def construct(self):
        # Titre
        titre = Text("Recherche dichotomique")
        self.play(Write(titre), run_time=3)
        self.wait(0.5)
        self.wait(1)
        self.play(FadeOut(titre, shift=3 * UP, run_time=1))

        # tableau initial
        N = 11
        tableau = sample(list(range(0, 100)), N)
        tableau.sort()

        # Dessin à l'écran
        cases = self.creation_tableau(tableau)
        cases = VGroup(*cases).move_to(ORIGIN + 2.5 * DOWN)
        self.play(LaggedStart(*[DrawBorderThenFill(c) for c in cases], lag_ratio=0.25, run_time=5))
        self.wait(1)

        # la cible
        # i_cible = randrange(0, N)
        i_cible = 3
        valeur_cible = tableau[i_cible]
        label_c = Text("cible : ")
        case_cible = Square(color=cases[i_cible][0][0].get_color(), fill_opacity=0.75).scale(0.6)
        case_cible.add(Text(str(valeur_cible)))
        case_cible.next_to(label_c, RIGHT)
        cible = VGroup(label_c, case_cible)
        cible.shift(3 * UP).align_to(cases, LEFT)
        self.play(FadeIn(cible), run_time=1)
        self.wait(1)
        comparateur = Text("<").next_to(case_cible, RIGHT)

        # Les flèches
        i_en_x = lambda i: cases[i].get_x()
        # début
        i_debut = 0
        debut = ValueTracker(i_en_x(i_debut))
        pointeur_d = Vector(0.5 * DOWN).move_to(cases[i_debut]).shift(1.25 * DOWN)
        label_d = Text("début", font="Consolas").scale(0.75).add_updater(lambda l: l.next_to(pointeur_d, UP))
        pointeur_d.add_updater(lambda p: p.move_to([debut.get_value(), 0, 0]).shift(1.25 * DOWN))
        texte_d = Text("début : " + str(i_debut).rjust(2), font="Consolas").align_to(label_c, DOWN).align_to(cases, RIGHT)

        def update_texte_d(o):
            o.become(Text("début : " + str(i_debut).rjust(2), font="Consolas").align_to(label_c, DOWN).align_to(cases, RIGHT))

        texte_d.add_updater(update_texte_d)

        # fin
        i_fin = N - 1
        fin = ValueTracker(i_en_x(i_fin))
        pointeur_f = Vector(0.5 * DOWN).move_to(cases[i_fin]).shift(1.25 * DOWN)
        label_f = (
            Text("fin", font="Consolas")
            .scale(0.75)
            .align_to(label_d, DOWN)
            .add_updater(lambda l: l.next_to(pointeur_f, UP))
        )
        pointeur_f.add_updater(lambda p: p.move_to([fin.get_value(), 0, 0]).shift(1.25 * DOWN))
        texte_f = Text("fin : " + str(i_fin).rjust(2), font="Consolas").next_to(texte_d, 6 * DOWN).align_to(texte_d, RIGHT)

        def update_texte_f(o):
            o.become(Text("fin : " + str(i_fin).rjust(2), font="Consolas").next_to(texte_d, 6 * DOWN).align_to(texte_d, RIGHT))

        texte_f.add_updater(update_texte_f)

        # Recherche dichotomique
        # fin
        i_milieu = i_debut + (i_fin - i_debut) // 2
        milieu = ValueTracker(i_en_x(i_milieu))
        texte_m = Text("milieu :  ?", font="Consolas").next_to(texte_d, 2 * DOWN).align_to(texte_d, RIGHT)

        # def update_texte_m(o):
        #     o.become(
        #         Text("milieu : " + str(i_milieu).rjust(2), font="Consolas").next_to(texte_d, 2 * DOWN).align_to(texte_d, RIGHT)
        #     )

        # texte_m.add_updater(update_texte_m)

        self.play(
            FadeIn(pointeur_d, label_d, pointeur_f, label_f), Write(texte_d), Write(texte_f), Write(texte_m), run_time=2
        )

        self.wait(1)

        while i_debut <= i_fin:
            i_milieu = i_debut + (i_fin - i_debut) // 2
            texte_m_=Text("milieu : " + str(i_milieu).rjust(2), font="Consolas").next_to(texte_d, 2 * DOWN).align_to(texte_d, RIGHT)
            self.pause(0.5)
            milieu.set_value(i_en_x(i_milieu))
            case_milieu_copie = cases[i_milieu][0].copy()
            self.play(
                ReplacementTransform(texte_m, texte_m_),
                Circumscribe(texte_m, color=GREEN_A),
                case_milieu_copie.animate.next_to(comparateur, RIGHT),
                run_time=2,
            )
            texte_m=texte_m_
            self.wait(1)

            if valeur_cible < tableau[i_milieu]:
                comparateur.become(Text("<").next_to(case_cible, RIGHT))
                self.play(FadeIn(comparateur))
                self.pause(1)
                i_fin_ = i_fin
                i_fin = i_milieu - 1
                texte_m_ = Text("milieu :  ?", font="Consolas").next_to(texte_d, 2 * DOWN).align_to(texte_d, RIGHT)
                self.play(
                    ReplacementTransform(texte_m, texte_m_),
                    Circumscribe(texte_f, color=GREEN_A),
                    fin.animate.set_value(i_en_x(i_fin)),
                    *[FadeToColor(cases[i], color=GRAY) for i in range(i_milieu, i_fin_ + 1)],
                    FadeOut(comparateur, case_milieu_copie),
                    run_time=2
                )
                texte_m = texte_m_
            elif valeur_cible == tableau[i_milieu]:
                comparateur.become(Text("=").next_to(case_cible, RIGHT))
                self.play(FadeIn(comparateur))
                self.wait(0.5)
                self.play(Indicate(case_cible), Indicate(case_milieu_copie), Indicate(comparateur))
                self.wait(1)
                break
            else:
                comparateur.become(Text(">").next_to(case_cible, RIGHT))
                self.play(FadeIn(comparateur))
                self.pause(1)
                i_debut_ = i_debut
                i_debut = i_milieu + 1
                # self.play(FadeOut(pointeur_m, label_m))
                texte_m_=Text("milieu :  ?", font="Consolas").next_to(texte_d, 2 * DOWN).align_to(texte_d, RIGHT)
                self.play(
                    ReplacementTransform(texte_m, texte_m_),
                    Circumscribe(texte_d, color=GREEN_A),
                    debut.animate.set_value(i_en_x(i_debut)),
                    *[FadeToColor(cases[i], color=GRAY) for i in range(i_debut_, i_milieu + 1)],
                    FadeOut(comparateur, case_milieu_copie),
                    run_time=2
                )
                texte_m = texte_m_
            self.wait(1)
