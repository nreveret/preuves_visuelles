from manim import (
    Scene,
    VGroup,
    Square,
    Text,
    MathTex,
    Write,
    PURE_GREEN,
    FadeOut,
    GrowFromPoint,
    RIGHT,
    LEFT,
    UP,
    TransformMatchingShapes,
    RED,
    WHITE,
    Rotate,
    PI,
    Brace,
    GrowFromCenter,
    Transform,
    ApplyWave,
)


def creation_boite(couleur, opacite, taille, label):
    resultat = VGroup()
    box = Square(side_length=taille, fill_color=couleur, fill_opacity=opacite, stroke_color=couleur)
    texte = MathTex(label).move_to(box.get_center())
    resultat.add(box, texte)
    return resultat


class Somme(Scene):
    def construct(self):
        self.wait(0.5)

        # Présentation
        somme_1_n = MathTex(r"1+2+3+4+\dots+n =\,?").scale(1.5)
        self.play(Write(somme_1_n, run_time=4))
        self.play(FadeOut(somme_1_n, run_time=1, shift=3 * UP))

        # Somme 1 + 2+ ... + 5
        x_1, y_1 = -6, 3.2
        taille_carres = 1
        couleur_carres = PURE_GREEN
        opacite = 0.4
        # 1
        un = MathTex("1").move_to([x_1, y_1, 0])
        carre_un = creation_boite(couleur_carres, opacite, taille_carres, "1").move_to([x_1, -3.2, 0])
        self.play(Write(un), GrowFromPoint(carre_un, un.get_center(), run_time=1.5))

        # 2
        deux = MathTex("+ 2").next_to(un)
        carre_deux_1 = carre_un.copy().next_to(carre_un, RIGHT, buff=0)
        carre_deux_2 = creation_boite(couleur_carres, opacite, taille_carres, "2").next_to(carre_deux_1, UP, buff=0)
        carres_deux = VGroup(carre_deux_1, carre_deux_2)
        self.play(Write(deux), GrowFromPoint(carres_deux, deux.get_center(), run_time=1.5))

        # 3
        trois = MathTex("+ 3").next_to(deux)
        carre_trois_1 = carre_deux_1.copy().next_to(carre_deux_1, RIGHT, buff=0)
        carre_trois_2 = carre_deux_2.copy().next_to(carre_deux_2, RIGHT, buff=0)
        carre_trois_3 = creation_boite(couleur_carres, opacite, taille_carres, "3").next_to(carre_trois_2, UP, buff=0)
        carres_trois = VGroup(carre_trois_1, carre_trois_2, carre_trois_3)
        self.play(Write(trois), GrowFromPoint(carres_trois, trois.get_center(), run_time=1.5))

        # 4
        quatre = MathTex("+ 4").next_to(trois)
        carre_quatre_1 = carre_trois_1.copy().next_to(carre_trois_1, RIGHT, buff=0)
        carre_quatre_2 = carre_trois_2.copy().next_to(carre_trois_2, RIGHT, buff=0)
        carre_quatre_3 = carre_trois_3.copy().next_to(carre_trois_3, RIGHT, buff=0)
        carre_quatre_4 = creation_boite(couleur_carres, opacite, taille_carres, "4").next_to(carre_quatre_3, UP, buff=0)
        carres_quatre = VGroup(carre_quatre_1, carre_quatre_2, carre_quatre_3, carre_quatre_4)
        self.play(Write(quatre), GrowFromPoint(carres_quatre, quatre.get_center(), run_time=1.5))
        somme_1_4 = VGroup(un, deux, trois, quatre)

        # Total vert
        pyramide_verte = VGroup(carre_un, carres_deux, carres_trois, carres_quatre)
        somme_1_4_parentheses = MathTex("(1+ 2+ 3+ 4)").move_to(un, LEFT)

        # 2nde pyramide
        self.play(TransformMatchingShapes(VGroup(un, deux, trois, quatre), somme_1_4_parentheses))
        somme_1_4_parentheses_bis = MathTex("+(1+ 2+ 3+ 4)").next_to(somme_1_4_parentheses, RIGHT)
        pyramide_rouge = pyramide_verte.copy().next_to(pyramide_verte, 4 * RIGHT)
        pyramide_rouge.set_color(RED).set_fill(RED)
        pyramide_rouge[0][1].set_color(WHITE)
        for elt in pyramide_rouge[1:]:
            for sous_elt in elt:
                sous_elt[1].set_color(WHITE)
        self.play(
            GrowFromPoint(somme_1_4_parentheses_bis, somme_1_4_parentheses.get_right()),
            GrowFromPoint(pyramide_rouge, pyramide_verte.get_right()),
            run_time=2,
        )
        self.wait(1)

        # Rotation
        labels = [pyramide_rouge[0][1]]
        for elt in pyramide_rouge[1:]:
            for sous_elt in elt:
                labels.append(sous_elt[1])

        self.play(Rotate(pyramide_rouge, PI), run_time=1)
        self.play(*[Rotate(obj, PI) for obj in labels], run_time=1)
        self.wait(1)

        pyramide_rouge_bis = pyramide_rouge.copy().next_to(carre_un, UP, buff=0).align_to(carre_un, LEFT)

        self.play(TransformMatchingShapes(pyramide_rouge, pyramide_rouge_bis))
        self.wait(1)

        # Mesures
        accolade_quatre = Brace(pyramide_rouge_bis, UP)
        quatre = MathTex("4").next_to(accolade_quatre, UP)
        groupe_quatre = VGroup(quatre, accolade_quatre)
        self.play(GrowFromCenter(groupe_quatre))

        accolade_cinq = Brace(VGroup(pyramide_verte, pyramide_rouge_bis), RIGHT)
        cinq = MathTex("4 + 1").next_to(accolade_cinq, RIGHT)
        groupe_cinq = VGroup(cinq, accolade_cinq)
        self.play(GrowFromCenter(groupe_cinq))
        self.wait(1)

        # aire du rectangle
        aire_rectangle = MathTex(r"= 4 \times (4 + 1)").next_to(somme_1_4_parentheses_bis, RIGHT)
        self.play(
            Transform(VGroup(quatre.copy(), cinq.copy()), aire_rectangle, replace_mobject_with_target_in_scene=True)
        )
        self.wait(1)

        # aire du triangle
        aire_triangle = MathTex(r"= \frac{4 \times (4+1)}{2}").next_to(somme_1_4, RIGHT)
        self.play(
            TransformMatchingShapes(somme_1_4_parentheses, somme_1_4),
            FadeOut(somme_1_4_parentheses_bis, shift=UP),
            FadeOut(pyramide_rouge_bis, shift=LEFT),
            FadeOut(groupe_cinq, shift=LEFT),
            FadeOut(groupe_quatre, shift=LEFT),
            TransformMatchingShapes(aire_rectangle, aire_triangle),
            run_time=2,
        )
        self.wait(1)

        # Conclusion
        self.play(*[FadeOut(mob) for mob in self.mobjects])
        somme_1_n = MathTex(r"1+2+3+4+\dots+n =\frac{n (n + 1)}{2}").scale(1.5)
        self.play(Write(somme_1_n, run_time=4))
        self.wait(1)
        self.play(ApplyWave(somme_1_n))

        self.wait(3)
