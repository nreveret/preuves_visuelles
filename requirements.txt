mkdocs-material>=8.2.1
mkdocs-awesome-pages-plugin>=2.7.0
mkdocs-material-extensions>=1.0.3
mkdocs-macros-plugin>=0.6.4
mkdocs-video